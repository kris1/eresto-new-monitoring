class AddPaymentDateToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :payment_date, :date
  end
end
