class ChangeColumnType < ActiveRecord::Migration[5.1]
  def change
    change_column :servers, :port, :integer
  end
end
