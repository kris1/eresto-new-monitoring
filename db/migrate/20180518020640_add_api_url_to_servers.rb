class AddApiUrlToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :api_url, :string
    add_column :servers, :email, :string
    add_index :servers, :email
  end
end
