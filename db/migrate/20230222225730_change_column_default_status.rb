class ChangeColumnDefaultStatus < ActiveRecord::Migration[5.1]
  def change
    change_column_default :charges, :status, default: 0
  end
end
