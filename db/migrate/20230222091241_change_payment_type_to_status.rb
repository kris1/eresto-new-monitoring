class ChangePaymentTypeToStatus < ActiveRecord::Migration[5.1]
  def change
    rename_column :charges, :payment_type, :status
  end
end
