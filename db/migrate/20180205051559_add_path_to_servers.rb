class AddPathToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :path, :string, default: 'eresto'
  end
end
