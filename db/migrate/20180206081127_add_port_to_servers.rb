class AddPortToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :port, :string, default: '22'
  end
end
