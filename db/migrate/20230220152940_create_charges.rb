class CreateCharges < ActiveRecord::Migration[5.1]
  def change
    create_table :charges do |t|
      t.references :server, foreign_key: true
      t.string :payment_desc
      t.integer :amount
      t.integer :payment_type

      t.timestamps
    end
  end
end
