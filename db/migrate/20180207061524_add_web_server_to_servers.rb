class AddWebServerToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :web_server, :string, default: 'passenger'
  end
end
