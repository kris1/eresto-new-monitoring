class AddClientImageToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :client_image, :string
  end
end
