class CreateServers < ActiveRecord::Migration[5.1]
  def change
    create_table :servers do |t|
      t.string :name
      t.string :dns
      t.string :username
      t.string :password

      t.timestamps
    end
    add_index :servers, :name

    # sql = "REPLACE INTO sqlite_sequence (name, seq) VALUES ('servers', 1000)"
    # ActiveRecord::Base.connection.execute(sql)
  end
end
