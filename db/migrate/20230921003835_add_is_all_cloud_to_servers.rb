class AddIsAllCloudToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :is_all_cloud, :boolean, defaulf: false
  end
end
