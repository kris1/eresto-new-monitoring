class AddSuperAdminUserNameToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :super_admin_username, :string
  end
end
