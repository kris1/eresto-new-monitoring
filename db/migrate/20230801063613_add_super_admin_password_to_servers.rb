class AddSuperAdminPasswordToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :super_admin_password, :string
  end
end
