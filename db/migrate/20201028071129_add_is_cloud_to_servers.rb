class AddIsCloudToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :is_cloud, :boolean, default: false
  end
end
