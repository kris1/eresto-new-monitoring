class AddIsPaidToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :is_paid, :boolean, default: false
  end
end
