class AddMapRouteToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :map_route, :string
  end
end
