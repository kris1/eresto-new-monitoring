class AddBranchToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :branch, :string, default: 'master'
  end
end
