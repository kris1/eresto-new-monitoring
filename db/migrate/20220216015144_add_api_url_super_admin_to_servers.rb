class AddApiUrlSuperAdminToServers < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :api_url_super_admin, :string
  end
end
