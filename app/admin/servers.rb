ActiveAdmin.register Server do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :dns, :username, :password, :branch, :path, :port, :web_server, :api_url, :email, :is_cloud, :is_all_cloud, :payment_date, :is_paid, :client_image, :map_route, :api_url_super_admin, :super_admin_password, :super_admin_username
  #
  # or
  #
  permit_params do
    permitted = [
      :name, 
      :dns, 
      :username, 
      :password, 
      :branch, 
      :path, 
      :port, 
      :web_server, 
      :api_url, 
      :email, 
      :is_cloud, 
      :is_all_cloud, 
      :payment_date, 
      :is_paid, 
      :client_image, 
      :map_route, 
      :api_url_super_admin, 
      :super_admin_password, 
      :super_admin_username]
    permitted << :other if params[:action] == 'create' && current_user.admin?
    permitted
  end

  index do
    selectable_column
    # id_column
    column :name
    column :dns
    column :api_url
    column :super_admin_username
    column :super_admin_password
    actions
  end

  filter :is_all_cloud
  filter :name

  form do |f|
    f.inputs do
      f.input :name
      f.input :dns
      f.input :username
      f.input :password
      f.input :branch
      f.input :path
      f.input :port
      f.input :web_server
      f.input :api_url
      f.input :email
      f.input :is_cloud
      f.input :is_paid
      f.input :is_all_cloud
      f.input :payment_date
      f.input :client_image, as: :file
      f.input :map_route
      f.input :api_url_super_admin
      f.input :super_admin_username
      f.input :super_admin_password
    end
    f.actions
  end
  
end
