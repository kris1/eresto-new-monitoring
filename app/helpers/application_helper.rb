module ApplicationHelper
  def active?(menu)
    'active' if request.path.match("/#{menu}")
  end

  def in_server_page?
    @server && @server.persisted?
  end

  def in_cloud_server_page?
    @server && @server.is_cloud == "true"
  end

  def payment_receive_image server
    if server.image.present?
      image_tag server.image_url
    else
      # Assuming you have a default.jpg in your assets folder
      image_tag 'default.jpg'
    end
  end
end
