class MaintenerGuidesController < ApplicationController
    before_action :block_page!

    def index
    end

    private
    
    def block_page!
        #   unless current_user.admin?
        #     redirect_to root_url, alert: 'You dont have access to this page'
        #   end
            redirect_to root_url, alert: 'This page under maintenance.'
    end

end
