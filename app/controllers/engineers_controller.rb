class EngineersController < ApplicationController
  before_action :authenticate_user!
  before_action :only_admin!
  before_action :set_engineer, except: [:index, :new, :create]

  def index
    @engineers = User.where.not(id: current_user.id).pluck(:id, :user_avatar, :name, :role)
  end

  def new
    @engineer = User.new
  end

  def create
    @engineer = User.new(engineer_params)

    if @engineer.save
      redirect_to engineers_url, notice: 'New engineer created!'
    else
      render :new
    end
  end

  def update
    if update_engineer
      redirect_to engineers_url, notice: 'Engineer data updated!'
    else
      render :edit
    end
  end

  def destroy
    @engineer.destroy
    redirect_to engineers_url, notice: 'Engineer data deleted!'
  end

  private
    def set_engineer
      @engineer = User.find(params[:id])
    end

    def engineer_params
      params.require(:user).permit(:email, :password, :name, :user_avatar).merge(role: 1)
    end

    def update_engineer
      if engineer_params[:password].blank?
        params[:user].delete('password')
        @engineer.update_without_password(engineer_params)
      else
        @engineer.update(engineer_params)
      end
    end

    def only_admin!
      unless current_user.admin?
        redirect_to root_url, alert: 'You dont have access to this page'
      end
    end
end
