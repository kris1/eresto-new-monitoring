class ChargesController < ApplicationController
  before_action :set_charge, only: [:show, :edit, :update, :destroy]

  # GET /charges
  def index
    @charges = Charge.all
    @total_charge = @charges.sum { |charge| charge.amount }
  end

  # GET /charges/1
  def show
    respond_to do |format|
      format.html
      format.json
      format.pdf { send_pdf }
    end
  end

  # GET /charges/new
  def new
    @charge = Charge.new
  end

  # GET /charges/1/edit
  def edit
  end

  # POST /charges
  def create
    @charge = Charge.new(charge_params)

    if @charge.save
      redirect_to charges_url, notice: 'Charge was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /charges/1
  def update
    if @charge.update(charge_params)
      redirect_to charges_url, notice: 'Charge was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /charges/1
  def destroy
    @charge.destroy
    redirect_to charges_url, notice: 'Charge was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_charge
      @charge = Charge.find(params[:id])
    end

    def send_pdf
      send_data @charge.receipt.render,
        filename: "#{@charge.created_at.strftime("%Y-%m-%d")}-gorails-receipt.pdf",
        type: "application/pdf",
        disposition: :inline # or :attachment to download
    end

    # Only allow a trusted parameter "white list" through.
    def charge_params
      params.require(:charge).permit(:server_id, :payment_desc, :amount, :status)
    end
end
