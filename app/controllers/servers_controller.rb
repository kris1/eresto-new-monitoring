class ServersController < ApplicationController
  before_action :authenticate_user!
  before_action :only_admin!, only: [:new, :create, :edit, :update]
  before_action :set_server, except: [:index, :new, :create, :cloud_server, :reset_all_paid, :import]
  before_action :check_api_url, only: :expiry_date

  def index
    # @servers = Server.where(is_cloud: false).pluck(:id, :name, :dns, :image, :branch)
    @servers = Server.where(is_cloud: false)
    # @filtered_data = Server.where params[:id]
    respond_to do |format|
      format.html
      format.xlsx
      format.csv { send_data Server.to_csv, filename: "servers-#{DateTime.now.strftime("%d%m%Y%H%M")}.csv"}
    end
  end

  def show

  end
 
  def cloud_server
    # @servers  = Server.where(is_cloud: true).pluck(:id, :name, :dns, :image, :branch)
    @servers  = Server.where(is_cloud: true, is_all_cloud: false)
  end

  def new
    @server = Server.new
  end

  def create
    @server = Server.new(server_params)

    if @server.save
      redirect_back(fallback_location: { action: "show", id: @server.id}, notice: 'Server was created!')
    else
      render :new
    end
  end

  def update
    params[:server].delete(:password) if server_params[:password].blank?
    if @server.update(server_params)
      # redirect_back(fallback_location: { action: "show", id: @server.id}, notice: "Server #{@server.name} updated!")
      redirect_to servers_path , notice: "Successfully update server."
    else
      render :edit
    end
  end

  def destroy
    @server.destroy
    redirect_to root_url, notice: "Server #{@server.name} deleted!"
  end

  def deploy
    ds = DeployService.new(@server, params[:app])
    Rails.cache.write('status', ds.update)
    redirect_to root_url, notice: "#{params[:app].upcase} of \"#{@server.name}\" updated!"
  end

  def restart_delayed_job
    ds = DeployService.new(@server, 'backend')
    Rails.cache.write('status', ds.restart_delayed_job)
    redirect_to root_url, notice: "DELAYED JOB of \"#{@server.name}\" restarted!"
  end

  def restart_puma
    ds = DeployService.new(@server, 'backend')
    Rails.cache.write('status', ds.restart_puma)
    redirect_to root_url, notice: "PUMA SERVER of \"#{@server.name}\" restarted!"
  end

  def delayed_job
    ds = DeployService.new(@server)
    Rails.cache.write('status', ds.delayed_job(dj_params))
    redirect_to root_url, notice: "DELAYED JOB of \"#{@server.name}\" #{dj_params}!"
  end

  def expiry_date
    if request.post?
      redirect_to request.env['HTTP_REFERER'], notice: @server.set_expiry_date(expiry_date_params, last_paid_at_params)
    else
      @data = @server.get_company_data
    end
  end

  def reset_all_paid
    @servers = Server.all
    @servers.update_all(is_paid: false)
    redirect_to root_url, notice: 'All server successfully to update paid status to false'
  end

  def import
    Server.import(params[:file])
    redirect_to root_url, notice: "Servers imported."
  end

  private
    def server_params
      params.require(:server).permit(:name, :dns, :username, :password, :branch, :path, :port, :web_server,
        :api_url, :email, :is_cloud, :is_all_cloud, :image, :payment_date, :is_paid, :client_image, :map_route, :api_url_super_admin, :super_admin_password, :super_admin_username)
    end

    def dj_params
      if params[:status].in?(['start', 'stop', 'restart'])
        params[:status]
      else
        'restart'
      end
    end

    def set_server
      @server = Server.find params[:id]
    end

    def expiry_date_params
      "#{params[:company]['expires(3i)']}-#{params[:company]['expires(2i)']}-#{params[:company]['expires(1i)']}"
    end

    def last_paid_at_params
      "#{params[:company]['last_paid_at(3i)']}-#{params[:company]['last_paid_at(2i)']}-#{params[:company]['last_paid_at(1i)']} #{params[:company]['last_paid_at(4i)']}:#{params[:company]['last_paid_at(5i)']}"
    end

    def check_api_url
      redirect_to root_url, alert: "No API URL on \"#{@server.name}\" server, please update the server data first." unless @server.api_url
    end

    def only_admin!
      unless current_user.admin?
        redirect_to root_url, alert: 'You dont have access to this page'
      end
    end
end
