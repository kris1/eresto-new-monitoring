class ConversationsController < ApplicationController
	before_action :block_page!

	def index
		session[:conversations] ||= []

		@users = User.all.where.not(id: current_user)
		@conversations = Conversation.includes(:recipient, :messages).find(session[:conversations])
	end

	def create
		@conversation = Conversation.get(current_user.id, params[:user_id])

		add_to_conversations unless conversated?

		respond_to do |format|
			format.js
		end
	end

	private

	def add_to_conversations
    	session[:conversations] ||= []
    	session[:conversations] << @conversation.id
	end

	def block_page!
    #   unless current_user.admin?
    #     redirect_to root_url, alert: 'You dont have access to this page'
    #   end
		redirect_to root_url, alert: 'This page under maintenance.'
    end

	def conversated?
		session[:conversations].include?(@conversation.id)
	end
end
