class DashboardPageController < ApplicationController
	before_action :authenticate_user!
    before_action :only_admin!
    
	def front_page
		@users = User.where(role: :network_engineer).order('created_at DESC')
	end

	private
	def only_admin!
      unless current_user.admin?
        redirect_to root_url, alert: 'You dont have access to this page'
      end
    end
end
