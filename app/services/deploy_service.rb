class DeployService
  def initialize(server, app='backend')
    @dns = server.dns
    @username = server.username
    @password = server.decrypted_password
    @branch = server.branch
    @path = server.path
    @port = server.port
    @web_server = server.web_server
    @app = app
  end

  def update
    set_env; `eval "$(ssh-agent -s)"; echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add - > /dev/null; ssh-add; cap backend deploy`
  end

  def restart_delayed_job
    set_env; `cap backend delayed_job:restart`
  end

  def restart_puma
    set_env; `cap backend puma:restart`
  end

  def delayed_job(command)
    set_env; `cap backend delayed_job:#{command}`
  end

  private
    def set_env
      ENV['CAP_DNS'] = @dns
      ENV['CAP_USERNAME'] = @username
      ENV['CAP_PASSWORD'] = @password
      ENV['CAP_APPLICATION'] = @app
      ENV['CAP_BRANCH'] = @branch
      ENV['CAP_REPO'] = repo_url
      ENV['CAP_PATH'] = @path
      ENV['CAP_PORT'] = @port
      ENV['CAP_WEB_SERVER'] = @web_server
    end

    # def deploy_file
    #   # @app.eql?('backend') ? 'backend' : 'frontend'
    #   @app.eql?('backend')
    # end

    def repo_url
      repos[@app]
    end

    def repos
      # @repos ||= {
      #   'backend' => 'git@gitlab.com:eresto/eresto-erp/eresto-backend-api.git',
      #   'manager' => 'git@gitlab.com:eresto/eresto-dist/eresto-manager-dist.git',
      #   'kitchen' => 'git@gitlab.com:eresto/eresto-dist/eresto-kitchen-dist.git',
      #   'pos'     => 'git@gitlab.com:eresto/eresto-dist/eresto-pos-dist.git'
      # }
      @repos ||= {
        'backend' => 'git@gitlab.com:eresto/eresto-erp/eresto-backend-api.git'
      }
    end
end
