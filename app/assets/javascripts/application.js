// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require jquery_ujs
//= require rails-ujs
// require turbolinks
// require_tree .

$(document).ready(function(){
  $('#body').slimScroll({
    height: '100vh',
    color: '#455A64',
    distance: '0',
    allowPageScroll: true
  });

  if($('#server-list')[0]){
    $('#server-list').DataTable()
  }

  if($('#engineer-list')[0]){
    $('#engineer-list').DataTable()
  }

  $('#togglePasswordLink').click(function(){
    currentText = $(this).text()
    toggleText = $(this).data('password')
    $(this).text(toggleText)
    $(this).data('password', currentText)
  })

  $(".reveal").on('click',function() {
      var $pwd = $(".pwd");
      if ($pwd.attr('type') === 'password') {
          $pwd.attr('type', 'text');
      } else {
          $pwd.attr('type', 'password');
      }
  });

  $('tbody').on('mouseover', 'tr', function () {
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover',
        html: true
    });
  });
})

$(document).on('click', '.toggle-window', function(e) {
  e.preventDefault();
  var panel = $(this).parent().parent();
  var messages_list = panel.find('.messages-list');

  panel.find('.panel-body').toggle();
  panel.attr('class', 'panel panel-default');

  if (panel.find('.panel-body').is(':visible')) {
    var height = messages_list[0].scrollHeight;
    messages_list.scrollTop(height);
  }
});
