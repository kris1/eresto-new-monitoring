# == Schema Information
#
# Table name: charges
#
#  id           :integer          not null, primary key
#  amount       :integer
#  payment_desc :string
#  status       :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  server_id    :integer
#
# Indexes
#
#  index_charges_on_server_id  (server_id)
#
class Charge < ApplicationRecord
  include ActionView::Helpers::NumberHelper

  belongs_to :server
  enum status: { cash: 0, transfer: 1 }

  def receipt
    Receipts::Invoice.new(
      details: [
        ["Receipt Number", "CH-00#{id}"],
        ["Date paid", Date.today],
        ["Payment Type", "<b><color rgb='#5eba7d'>#{status}</color></b>"]
        ],
        company: {
          name: "PT.Eresto Digital Indonesia",
          address: "Komp.Kota Kembang Permai\n kav 1 No.20-22\nBandung, 40271",
          email: "support@eresto.co.id",
          logo: File.expand_path("./app/assets/images/eresto.png")
        },
        recipient: [
          "<b>#{server.name}</b>",
        ],
        line_items: [
          ["<b>Item</b>", "<b>Unit Cost</b>", "<b>Quantity</b>", "<b>Amount</b>"],
          ["Maintenance", "#{total_in_currency}", "1", "#{total_in_currency}"],
          [nil, nil, "Subtotal", "#{total_in_currency}"],
          [nil, nil, "Tax", "Rp.0"],
          [nil, nil, "Total", "#{total_in_currency}"],
          [nil, nil, "<b>Amount paid</b>", "<b>#{total_in_currency}</b>"],
          # [nil, nil, "Refunded on #{Date.today}", "$5.00"]
        ],
        footer: "For questions, contact us anytime at <b>support@eresto.co.id</b>"
      )
  end

  def total_in_currency
    number_to_currency(amount, unit: 'Rp', precision: 2)
  end
end
