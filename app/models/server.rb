# == Schema Information
#
# Table name: servers
#
#  id                   :integer          not null, primary key
#  api_url              :string
#  api_url_super_admin  :string
#  branch               :string           default("master")
#  client_image         :string
#  dns                  :string
#  email                :string
#  is_all_cloud         :boolean
#  is_cloud             :boolean          default(FALSE)
#  is_paid              :boolean          default(FALSE)
#  map_route            :string
#  name                 :string
#  password             :string
#  path                 :string           default("eresto")
#  payment_date         :date
#  port                 :integer          default(22)
#  super_admin_password :string
#  super_admin_username :string
#  username             :string
#  web_server           :string           default("passenger")
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_servers_on_email  (email)
#  index_servers_on_name   (name)
#

class Server < ApplicationRecord
  # attr_accessor :name, :dns, :username, :password, :branch, :path, :port, :web_server, :api_url, :is_cloud, :is_paid
  validates :name, :dns, :username, :password, presence: true
  before_save :encrypt_password, if: :password_changed?
  has_many :charges

  validates :client_image, allow_blank: true, format: {
        with: %r{\.(gif|jpg|png)\z}i,
        message: 'must be a URL for GIF, JPG or PNG image.'
  }

  def encrypt_password
    self.password = Server.encrypt(password)
  end

  def decrypted_password
    Server.decrypt(password)
  end

  def self.encrypt(message)
    AESCrypt.encrypt(message, secret_key)
  end

  def self.decrypt(message)
    AESCrypt.decrypt(message, secret_key)
  end

  def http_url
    # if port == '22'
    #   "http://#{dns}"
    # else
    #   "http://#{dns}:#{port}"
    # end
    if self.is_cloud?
      "https://#{api_url}"
    else  
      "http://#{api_url}"
    end
  end

  def return_json(request)
    json = JSON.parse request.response_body
    unless json.key?("company")
      json["company"] = json
    end
    json
  end

  def authenticate_user
    login_params = {'user[email]' => 'eresto@eresto.com', 'user[password]' => 'secret1987'}
    req = Typhoeus.post(http_url + '/v1/sessions', {body: login_params})
    return_json req
  end

  def set_expiry_date(expiry_date, last_paid_at)
    user_data = authenticate_user
    header_params = {
      'Authorization' => "Token token=#{user_data['token']}, email=#{user_data['email']}"
    }
    company_params = {
      company: {
        expires: expiry_date,
        last_paid_at: last_paid_at
      }
    }
    req = Typhoeus.patch(http_url + "/v2/companies/" + user_data['company_id'], headers: header_params, body: company_params)
    if req.response_code == 200
      'Expiry date successfully updated!'
    else
      return_json req
    end
  end

  def get_company_data
    user_data = authenticate_user
    header_params = {
      'Authorization' => "Token token=#{user_data['token']}, email=#{user_data['email']}"
    }
    req = Typhoeus.get(http_url + "/v2/companies/" + user_data['company_id'], headers: header_params)
    if req.response_code == 200
      # binding.pry
      return_json req
    else
      req.response_body
    end
  end

  def self.to_csv
    servers = all
    CSV.generate do |csv|
      csv << column_names
      servers.each do |server|
        csv << server.attributes.values_at(*column_names)
      end
    end
  end

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      server = find_by_id(row["id"]) || new
      server.attributes = row.to_hash.slice(*row.to_hash.keys)
      server.save!
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  private
    def self.secret_key
      # Rails.application.secrets.secret_key_base
      '894b7ce2b407f07a7ccca9cea388be55330bc939f19212be892e616a7bab1cf53f94dd7630193c4f973d6910f7b7f677da9373ccd44eab3a780d8f6ec2e7f335'
    end
end
