Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :charges
  devise_for :users,
    controllers: { sessions: 'users/sessions' },
    path_names: { sign_in: 'login', sign_out: 'logout' }

  root 'servers#index'
  resources :servers do
    member do
      get :monitor
      get :router
      # get :config
      get :expiry_date
      post :restart_delayed_job
      post :restart_puma
      post 'delayed_job/:status', to: 'servers#delayed_job', as: :delayed_job
      post :expiry_date
      post 'deploy/:app', to: 'servers#deploy', as: :deploy
    end
    collection do
      get :cloud_server
      get :reset_all_paid
      post :import 
    end
  end
  
  resources :conversations, only: [:create]

  get 'dashboard_page/front_page'
  get 'conversations/index'
  get 'maintener_guides/index'

  resources :servers
  resources :engineers
end
